## Лабораторная работа № 5

### Код тест кейсов

```
#!/usr/bin/python3.5
# -*- coding: utf-8 -*-


from order_tracker.exceptions.exceptions import UserError, TaskError, ConfigError
from order_tracker.reminder_manager.reminder_manager import ReminderManager
from order_tracker.order_manager.order_manager import OrderManager
from order_tracker.user_manager.user_manager import UserManager
from order_tracker.exceptions.exceptions import TaskError
from order_tracker.reminder.reminder import Reminder
from order_tracker.storage.storage import Storage
from order_tracker.orders.order import Order
from datetime import datetime, timedelta
from order_tracker.user.user import User
import unittest
import tempfile
import shutil
import json
import os


class UnitTests(unittest.TestCase):
    def setUp(self):
        self.__tracker_dir = tempfile.mkdtemp()
        self.info_storage = Storage(self.__tracker_dir + '/', self.__tracker_dir + '/', self.__tracker_dir + '/')
        self.order_manager = OrderManager(self.info_storage)
        self.user_manager = UserManager(self.info_storage)
        self.reminder_manager = ReminderManager(self.info_storage)
        with open(os.path.join(self.info_storage.r_path, 'reminder.json'), 'w') as j_file:
            json.dump([], j_file)
        with open(os.path.join(self.info_storage.t_path, 'sub_orders.json'), 'w') as j_file:
            json.dump([], j_file)
        with open(os.path.join(self.info_storage.t_path, 'orders.json'), 'w') as j_file:
            json.dump([], j_file)
        with open(os.path.join(self.info_storage.us_path, 'users.json'), 'w') as j_file:
            json.dump({}, j_file)

    def tearDown(self):
        shutil.rmtree(self.__tracker_dir)

    def test_create_user(self):
        """Create new user"""
        user = User('artem', 'password', 'Artem')
        self.user_manager.create_new_user(user.login, user.password, user.name)
        content = {'artem': {
            'password': 'password',
            'user_personality': 'Artem'
        }}
        users_dict = self.info_storage.load_user_by_id('artem')
        self.assertEqual(users_dict, content)

    def test_authorization(self):
        """Auth new user"""
        user = User('artem', 'password', 'Artem')
        self.user_manager.create_new_user(user.login, user.password, user.name)
        self.assertTrue(self.user_manager.authorization(user.login, user.password))

    def test_create_new_order(self):
        """Create new order"""
        order = Order('Task1', 1, 'Work', None, 'artem')
        content = {
                    'content': 'Task1',
                    'date_of_create': datetime.now().strftime('%d.%m.%Y %H:%M'),
                    'execution_date': (datetime.now() + timedelta(days=7)).strftime('%d.%m.%Y %H:%M'),
                    'group': 'Work',
                    'id': 1,
                    'priority': 1,
                    'status': 'Created',
                    'owner': 'artem'
                    }
        self.order_manager.create_new_order('artem', False, None, order.content,
                                          order.priority, order.group, order.execution_date)
        orders_dict = self.info_storage.load_order_by_id('artem', False, 1)
        self.assertEqual(orders_dict, content)

    def test_edit_existing_order(self):
        """Edit existing order by user"""
        order = Order('Task1', 1, 'Work', None, 'artem')
        content = {
                    'content': 'Task1',
                    'date_of_create': datetime.now().strftime('%d.%m.%Y %H:%M'),
                    'execution_date': (datetime.now() + timedelta(days=7)).strftime('%d.%m.%Y %H:%M'),
                    'group': 'Work',
                    'id': 1,
                    'priority': 4,
                    'status': 'Done',
                    'owner': 'artem'
                    }
        self.order_manager.create_new_order('artem', False, None, order.content, order.priority,
                                          order.group, order.execution_date)
        self.order_manager.edit_order('artem', False, 1, None, 4, None, None, 'Done')
        orders_dict = self.info_storage.load_order_by_id('artem', False, 1)
        self.assertEqual(orders_dict, content)

    def test_remove_order(self):
        """Remove order by user"""
        order = Order('Task1', 1, 'Work', None, 'artem')
        self.order_manager.create_new_order('artem', False, None, order.content, order.priority,
                                          order.group, order.execution_date)
        self.order_manager.remove_order('artem', False, 1)
        orders_dict = self.info_storage.load_order_by_id('artem', False, 1)
        self.assertFalse(orders_dict)

    def test_create_sub_order(self):
        """Create suborder for components"""
        order = Order('sub1', 3, 'Personal', None, 'artem')
        content = {
                    'content': 'sub1',
                    'date_of_create': datetime.now().strftime('%d.%m.%Y %H:%M'),
                    'execution_date': (datetime.now() + timedelta(days=7)).strftime('%d.%m.%Y %H:%M'),
                    'group': 'Personal',
                    'id': 1,
                    'priority': 3,
                    'status': 'Created',
                    'owner': 'artem',
                    'parent_id': 1
            }
        self.order_manager.create_new_order('artem', True, 1, order.content, order.priority,
                                          order.group, order.execution_date)
        orders_dict = self.info_storage.load_order_by_id('artem', True, 1)
        self.assertEqual(orders_dict, content)

    def test_edit_sub_order(self):
        """Edit components suborder"""
        order = Order('sub', 1, 'Work', None, 'artem')
        content = {
                'content': 'SUB',
                'date_of_create': datetime.now().strftime('%d.%m.%Y %H:%M'),
                'execution_date': (datetime.now() + timedelta(days=7)).strftime('%d.%m.%Y %H:%M'),
                'group': 'Personal',
                'owner': 'artem',
                'parent_id': 1,
                'id': 1,
                'priority': 2,
                'status': 'Created'
                }
        self.order_manager.create_new_order('artem', True, 1, order.content, order.priority,
                                          order.group, order.execution_date)
        self.order_manager.edit_order('artem', True, 1, 'SUB', 2, 'Personal', None, None)
        orders_dict = self.info_storage.load_order_by_id('artem', True, 1)
        self.assertEqual(orders_dict, content)

    def test_remove_sub_order(self):
        """Remove component suborder"""
        order = Order('Something', 1, 'Work', None, 'artem')
        content = [
            {
                "content": "sub1",
                "date_of_create": datetime.now().strftime('%d.%m.%Y %H:%M'),
                "execution_date": (datetime.now() + timedelta(days=7)).strftime('%d.%m.%Y %H:%M'),
                "group": "Personal",
                'owner': 'artem',
                'parent_id': 2,
                "id": 1,
                "priority": 1,
                "status": "Created"
            },
            {
                "content": "sub2",
                "date_of_create": datetime.now().strftime('%d.%m.%Y %H:%M'),
                "execution_date": (datetime.now() + timedelta(hours=12)).strftime('%d.%m.%Y %H:%M'),
                "group": "Personal",
                'owner': 'artem',
                'parent_id': 2,
                "id": 2,
                "priority": 1,
                "status": "Done"
            },
            {
                "content": "sub2",
                "date_of_create": datetime.now().strftime('%d.%m.%Y %H:%M'),
                "execution_date": (datetime.now() + timedelta(days=41)).strftime('%d.%m.%Y %H:%M'),
                "group": "Personal",
                'owner': 'artem',
                'parent_id': 2,
                "id": 3,
                "priority": 2,
                "status": "Created"
            }
            ]
        self.order_manager.create_new_order('artem', True, 2, 'sub1', 1, 'Personal', order.execution_date)
        self.order_manager.create_new_order('artem', True, 2, 'sub2', 1, 'Personal',
                                          (datetime.now() + timedelta(hours=12)).strftime('%d.%m.%Y %H:%M'))
        self.order_manager.create_new_order('artem', True, 2, 'sub2', 2, 'Personal',
                                          (datetime.now() + timedelta(days=41)).strftime('%d.%m.%Y %H:%M'))
        self.order_manager.create_new_order('artem', True, 2, order.content, order.priority, order.group,
                                          order.execution_date)
        self.order_manager.edit_order('artem', True, 2, None, None, None, None, 'Done')
        self.order_manager.remove_order('artem', True, 4)
        orders_dict = self.info_storage.load_user_orders(True, 'artem')
        self.assertEqual(orders_dict, content)

    def test_edit_order_without_rights(self):
        """Try to edit order without rights"""
        order = Order('Task1', 1, 'Work', None, 'artem')
        self.order_manager.create_new_order('artem', False, None, order.content, order.priority,
                                          order.group, order.execution_date)
        with self.assertRaises(UserError):
            self.info_storage.load_order_by_id('NotArtem', False, 1)

    def test_edit_nonexistent_order(self):
        """Try to edit nonexistent order"""
        with self.assertRaises(TaskError):
            self.order_manager.edit_order('artem', False, 2, 'something', 1, None, None, 'Done')

    def test_remove_nonexistent_order(self):
        with self.assertRaises(TaskError):
            self.order_manager.remove_order('artem', False, 1)

    def test_create_existing_user(self):
        user = User('artem', 'password', 'Artem')
        self.user_manager.create_new_user(user.login, user.password, user.name)
        with self.assertRaises(UserError):
            self.user_manager.create_new_user(user.login, user.password, user.name)

    def test_authorization_fail(self):
        user = User('artem', 'password', 'Artem')
        self.user_manager.create_new_user(user.login, user.password, user.name)
        user.login = 'artem1'
        with self.assertRaises(UserError):
            self.user_manager.authorization(user.login, user.password)

    def test_incorrect_date_of_create(self):
        with self.assertRaises(TaskError):
            Order('content', 1, 'Work', [(datetime.today() - timedelta(days=1)).strftime('%d.%m.%Y'),
                                        datetime.now().strftime('%H:%M')], 'artem')

    def test_remove_user_with_orders(self):
        order = Order('TAsk1', None, None, None, 'artem')
        self.order_manager.create_new_order('artem', False, None, order.content, order.priority,
                                          order.group, order.execution_date)
        order.content = 'Sub1'
        self.order_manager.create_new_order('artem', True, 1, order.content, order.priority,
                                          order.group, order.execution_date)
        self.order_manager.remove_order('artem', False, 1)
        order = self.info_storage.load_order_by_id('artem', False, 1)
        sub = self.info_storage.load_order_by_id('artem', True, 1)
        self.assertFalse(order)
        self.assertFalse(sub)

    def test_edit_responsible_order(self):
        order = Order('Task1', 1, 'Work', None, 'artem')
        order_content = {
            'content': 'Task1',
            'date_of_create': datetime.now().strftime('%d.%m.%Y %H:%M'),
            'execution_date': (datetime.now() + timedelta(days=7)).strftime('%d.%m.%Y %H:%M'),
            'group': 'Work',
            'id': 1,
            'priority': 4,
            'status': 'Done',
            'owner': 'artem'
        }
        self.order_manager.create_new_order('artem', False, None, order.content, order.priority,
                                          order.group, order.execution_date)
        sub = Order('sub1', 3, 'Personal', None, 'artem')
        sub_content = {
            'content': 'sub1',
            'date_of_create': datetime.now().strftime('%d.%m.%Y %H:%M'),
            'execution_date': (datetime.now() + timedelta(days=7)).strftime('%d.%m.%Y %H:%M'),
            'group': 'Personal',
            'id': 1,
            'priority': 3,
            'status': 'Done',
            'owner': 'artem',
            'parent_id': 1
        }
        self.order_manager.create_new_order('artem', True, 1, sub.content, sub.priority,
                                          sub.group, sub.execution_date)
        self.order_manager.edit_order('artem', False, 1, None, 4, None, None, 'Done')
        orders_dict = self.info_storage.load_order_by_id('artem', False, 1)
        sub_dict = self.info_storage.load_order_by_id('artem', True, 1)
        self.assertEqual(orders_dict, order_content)
        self.assertEqual(sub_dict, sub_content)

    def test_edit_user_order_by_manager(self):
        order = Order('Task1', 1, 'Work', None, 'artem')
        order_content = {
            'content': 'Task1',
            'date_of_create': datetime.now().strftime('%d.%m.%Y %H:%M'),
            'execution_date': (datetime.now() + timedelta(days=7)).strftime('%d.%m.%Y %H:%M'),
            'group': 'Work',
            'id': 1,
            'priority': 4,
            'status': 'Failed',
            'owner': 'artem'
        }
        self.order_manager.create_new_order('artem', False, None, order.content, order.priority,
                                          order.group, order.execution_date)
        sub = Order('sub1', 3, 'Personal', None, 'artem')
        sub_content = {
            'content': 'sub1',
            'date_of_create': datetime.now().strftime('%d.%m.%Y %H:%M'),
            'execution_date': (datetime.now() + timedelta(days=7)).strftime('%d.%m.%Y %H:%M'),
            'group': 'Personal',
            'id': 1,
            'priority': 3,
            'status': 'Failed',
            'owner': 'artem',
            'parent_id': 1
        }
        self.order_manager.create_new_order('artem', True, 1, sub.content, sub.priority,
                                          sub.group, sub.execution_date)
        self.order_manager.edit_order('artem', False, 1, None, 4, None, None, 'Failed')
        orders_dict = self.info_storage.load_order_by_id('artem', False, 1)
        sub_dict = self.info_storage.load_order_by_id('artem', True, 1)
        self.assertEqual(orders_dict, order_content)
        self.assertEqual(sub_dict, sub_content)

    def test_add_reminder(self):
        order = Order('Task1', 1, 'Work', None, 'artem')
        self.order_manager.create_new_order('artem', False, None, order.content,
                                          order.priority, order.group, order.execution_date)
        reminder = Reminder('WR', 'artem', None)
        self.reminder_manager.create_new_reminder(None, reminder.owner, reminder.week_day, False, reminder.frequency)
        new_reminder = {'order_id': 1,
                        'owner': 'artem',
                        'parent_id': None,
                        'frequency': 'WR',
                        'id': 1,
                        'week_day': None,
                        'last_update': datetime.today().strftime('%d.%m.%Y')
                        }
        reminder = self.info_storage.load_order_reminder(1, None, 'artem')
        self.assertEqual(reminder, new_reminder)

    def test_remove_reminder(self):
        order = Order('Task1', 1, 'Work', None, 'artem')
        self.order_manager.create_new_order('artem', False, None, order.content,
                                          order.priority, order.group, order.execution_date)
        reminder = Reminder(None, 'artem', None)
        self.reminder_manager.create_new_reminder(None, reminder.owner, reminder.week_day, False, reminder.frequency)
        self.reminder_manager.remove_reminder(False, None, 1, 'artem')
        reminder = self.info_storage.load_order_reminder(1, None, 'artem')
        self.assertFalse(reminder)

    def test_edit_order_by_admin(self):
        order = Order('Task1', 1, 'Work', None, 'artem')
        self.order_manager.create_new_order('artem', False, None, order.content,
                                          order.priority, order.group, order.execution_date)
        reminder = Reminder('WR', 'artem', None)
        self.reminder_manager.create_new_reminder(None, reminder.owner, reminder.week_day, False, reminder.frequency)
        new_reminder = {'order_id': 1,
                        'owner': 'artem',
                        'id': 1,
                        'parent_id': None,
                        'frequency': 'WR',
                        'week_day': None,
                        'last_update': datetime.today().strftime('%d.%m.%Y')
                        }
        sub = Order('Sub1', 1, 'Work', None, 'artem')
        self.order_manager.create_new_order('artem', True, 1, sub.content, sub.priority, sub.group, sub.execution_date)
        sub_reminder = Reminder('ED', 'artem', None)
        self.reminder_manager.create_new_reminder(1, sub_reminder.owner, sub_reminder.week_day,
                                                  True, sub_reminder.frequency)
        sub_reminder = {'order_id': 1,
                        'owner': 'artem',
                        'id': 2,
                        'parent_id': 1,
                        'frequency': 'ED',
                        'week_day': sub_reminder.week_day,
                        'last_update': datetime.today().strftime('%d.%m.%Y')
                        }
        reminder = self.info_storage.load_order_reminder(1, None, 'artem')
        new_sub_rem = self.info_storage.load_order_reminder(1, 1, 'artem')
        self.assertEqual(reminder, new_reminder)
        self.assertEqual(new_sub_rem, sub_reminder)

```

## Результат выполнения тестов

![](media/tests_result.png)
