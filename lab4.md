## Лабораторная работа №4

#### Component diagramm

![](media/Component_diagramm.png)

### SD

ADMIN USE CASE:
![](media/Admin_use_case.png)

MANAGER USE CASE:
![](media/Manager_use_case.png)

USER USE CASE:
![](media/User_use_case.png)

### Создание заявки пользователем

![](media/SCD_user_create_order.png)

### Авторизация пользователя

![](media/SCD_user_auth.png)

### Диаграмма классов

![](media/Class_diagramm.png)

### DLL

```
create table category (
    id integer primary key not null,
    name text not null,
    type text,
);

create table user (
    id integer primary key not null,
    fullname text not null,
    mobilephone text not null,
    email text not null,
);

create table type (
    id integer primary key not null,
    name text not null,
);

create table components (
    id integer primary key not null,
    count integer not null,
    description str not null,
    type integer references type(id),
);

create table order (
    id integer primary key not null,
    [components] integer references component(id),
    [group] integer references groups(id),
    owner integer references user(id),
    object integer references object(id),
    responsible integer references user(id),
    category integer references category(id),
    preparation_time integer,
    status text not null,
);
create table object (
    id integer primary key not null,
    name text not null,
    Description text not null,

);
```
